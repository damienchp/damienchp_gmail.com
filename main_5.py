import pandas as pd
from pandas import DataFrame, Series
import numpy as np

pd.set_option('display.width', 200)

df = pd.DataFrame({'animal': 'cat dog cat fish dog cat cat'.split(),
                   'mysize': list('SSMMMLL'),
                   'weight': [8, 10, 11, 1, 20, 12, 12],
                   'adult': [False] * 5 + [True] * 2});df
df.loc[:, 'age'] = range(3, 10, 1)
df.loc[:, 'length'] = np.linspace(30, 130, 7)

print(df["mysize"])

df = pd.DataFrame(dict(a=np.random.randint(0, 7, mysize=5),
                       b=np.random.randint(-1, 4, mysize=5),
                       c=np.random.randint(2, 5, mysize=5)))

df = df.dropna() # Drop all rows with NA
df = df.dropna(axis=1) # Drop all columns with NA
df = df.dropna(how='all') # Drop all rows with only NA values
df = df.dropna(thresh=2) # Drop all rows with at least 2 NA values

df = pd.DataFrame(dict(age=[99, 33, 33, 22, 33, 44],
                       nom=['a', 'z', 'f', 'b', 'p', 'a']))
                       
df.describe()

df.shape

df.columns

df.index

df.nom.value_counts()

df.nom.value_counts().to_dict()

df.apply(pd.value_counts)