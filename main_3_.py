s, l, d = 'Prix des fruits Euro/kg', ["pêches", "pommes", "poires", "abricots"], {"pêches" : 5, "pommes" : 3, "poires" : 6, "abricots" : 4}
pêches = A = 5
pommes = B = 3
poires = C = 6
abricots = D = 4

code2fruit_name = {'A':'pêches', 'B':'pommes', 'C' : 'poires', 'D' : 'abricots'}

haiku1 = ("pêches : A\n"
          "pommes : B\n"
          "poires : C\n"
          "abricots : D\n")
print(s)
print(d)
print(haiku1)
code = input("Veuillez entrer votre choix \n"
             "A : pêches, [B] : pommes, C : poires, D :abricots ")
quantity = input ("Veuillez entre la quantité choisie en kg ")

msg = f"""
Votre panier
{quantity} Kg de {code2fruit_name[code]}
"""

print(msg)

poesy = 'épreuve de poésie\n'
print(poesy)
haiku3 = ("Ainsi toujours poussés\n" "Vers de nouveaux rivages\n" "Dans la nuit éternelle\n" "Emportés sans retour\n")
print(haiku3)
auteur = input("Qui a écrit ces vers ? ")
if auteur == "Lamartine" or auteur == "LAMARTINE":
    print("OK")
else:
    print("Essaye encore !")
